package com.ideacellular.ideamoney.views.dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.ideacellular.ideamoney.R;

/**
 * Created by manishp on 7/1/16.
 */
public class CustomDialog extends AppCompatDialog {

    private final Context mContext;
    private String mTitle;
    private String mInformation;
    private OnDialogClicked mOnDialogClicked;

    public CustomDialog(Context context, String mTitle, String mInformation, OnDialogClicked mOnDialogClicked) {

        super(context);
        this.mContext = context;
        this.mTitle = mTitle;
        this.mInformation = mInformation;
        this.mOnDialogClicked = mOnDialogClicked;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.success_dialog);
        setCancelable(false);
        initView();
    }

    private void initView() {
        TextView title = (TextView) findViewById(R.id.success_title);
        title.setText(mTitle);

        TextView info = (TextView) findViewById(R.id.information);
        info.setText(mInformation);

        Button ok = (Button) findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mOnDialogClicked) {
                    mOnDialogClicked.onPositiveButtonClick(CustomDialog.this);
                } else {
                    dismiss();
                }
            }
        });
    }

    @Override
    public void show() {

        if (mContext instanceof Activity) {
            if (!((Activity) mContext).isFinishing()) {
                super.show();
            }
        } else {
            super.show();
        }
    }


    public interface OnDialogClicked {
        void onPositiveButtonClick(DialogInterface dialog);
    }
}
