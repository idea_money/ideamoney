package com.ideacellular.ideamoney.worklight.challenge;

import android.content.Context;
import android.util.Log;

import com.worklight.wlclient.api.WLFailResponse;
import com.worklight.wlclient.api.WLResponse;
import com.worklight.wlclient.api.challengehandler.ChallengeHandler;

/**
 * Created by manishp on 5/11/15.
 */
public class AuthChallengeHandler extends ChallengeHandler {

    private static final String TAG = "AuthChallengeHandler";
    private Context mContext;
    private int mAuthCount;

    public AuthChallengeHandler(String realm, Context context) {
        super(realm);
        mContext = context;
    }

    @Override
    public boolean isCustomResponse(WLResponse response) {
        Log.e(TAG, "In isCustomResponse...");
        return true;
    }

    @Override
    public void handleChallenge(WLResponse response) {
        Log.e(TAG, "In handleChallenge...");
    }

    @Override
    public void onSuccess(WLResponse wlResponse) {
        mAuthCount = 0;
        submitSuccess(wlResponse);
    }

    @Override
    public void onFailure(WLFailResponse wlFailResponse) {
        submitFailure(wlFailResponse);
    }
}
