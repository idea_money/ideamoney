package com.ideacellular.ideamoney;

import android.app.Application;

/**
 * Created by avanin on 21/4/16.
 */
public class IdeaMoneyApplication extends Application {
    private static IdeaMoneyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static IdeaMoneyApplication getInstance() {
        return mInstance;
    }
}
