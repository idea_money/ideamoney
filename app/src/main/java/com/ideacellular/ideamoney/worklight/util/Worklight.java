package com.ideacellular.ideamoney.worklight.util;

import android.content.Context;
import android.util.Log;

/**
 * Created by manishp on 5/11/15.
 */
public class Worklight {

    /**
     * Worklight
     */
    public static final int WORKLIGHT_REQUEST_TIME_OUT = 35000;
    public static final int WORKLIGHT_BILLPLAN_TIME_OUT = 60000;
    public static final String IDEA_DSM_ADAPTER = "ideaDSMAppAdapter";


    /**
     * Procedures
     */


    /**
     * Generates instance of worklight and registers the challenge handler.
     *
     * @param context Activity context
     */
    public static void initWorklight(Context context) {
        WLClient client = WLClient.createInstance(context);
    /*    AuthChallengeHandler authChallengeHandler = new AuthChallengeHandler(context.getString(R.string.single_step_auth_realm), context);
        client.registerChallengeHandler(authChallengeHandler);*/
        com.worklight.common.Logger.setContext(context);
    }

    public static synchronized void invokeProcedure(final String procedureName, final Object[] parameters, ServerUtil.OnResponseListener onResponseListener, final Context context) {

        try {
            Log.i("Worklight", "Procedure name " + procedureName);

            WLClient client = WLClient.getInstance();

            invoke(procedureName, Worklight.IDEA_DSM_ADAPTER, parameters, new ResponseHandler(onResponseListener), client, context, Worklight.WORKLIGHT_REQUEST_TIME_OUT);

        } catch (RuntimeException e) {

            if (handleWorklightException(e)) {
                WLClient client = WLClient.createInstance(context);
                invoke(procedureName, Worklight.IDEA_DSM_ADAPTER, parameters, new ResponseHandler(onResponseListener), client, context, Worklight.WORKLIGHT_REQUEST_TIME_OUT);
            } else {

                Log.v("TAG", "invokeProcedure:RuntimeException" + e.getMessage());
            }
        }
    }

    private static void invoke(final String procedureName, final String adapterName, final Object[] parameters, final WLResponseListener listener, final WLClient client, final Context context, int timeout) {

        WLProcedureInvocationData invocationData = new WLProcedureInvocationData(adapterName, procedureName);
        invocationData.setParameters(parameters);
        invocationData.setCompressResponse(true);
        WLRequestOptions options = new WLRequestOptions();
        options.setTimeout(timeout);
        client.invokeProcedure(invocationData, listener, options);

    }

    private static boolean handleWorklightException(RuntimeException e) {
        return e.getMessage().contains("You must call WLClient.createInstance first.");
    }

}
