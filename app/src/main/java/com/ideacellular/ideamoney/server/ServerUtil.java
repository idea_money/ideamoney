package com.ideacellular.ideamoney.server;

import android.content.Context;
import android.util.Log;

import com.ideacellular.ideamoney.worklight.util.Worklight;

/**
 * Created by manishp on 6/11/15.
 */
public class ServerUtil {

    private static final String TAG = ServerUtil.class.getSimpleName();

    //Demo API Call

//    public static void getCallHistory(final String number, final String circle, final String lob, final String channelType, OnResponseListener onResponseListener, Context context) {
//
//        Object[] parameters = new Object[]{number, circle, lob, channelType};
//        Worklight.invokeProcedure(Worklight.PROCEDURE_CALL_HISTORY, parameters, onResponseListener, context);
//
//    }

    public interface OnResponseListener {
        void onSuccess(String response);

        void onFailure(String response);
    }
}
