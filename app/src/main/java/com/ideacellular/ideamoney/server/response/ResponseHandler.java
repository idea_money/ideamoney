package com.ideacellular.ideamoney.server.response;

import android.util.Log;

import com.ideacellular.ideamoney.server.ServerUtil;
import com.worklight.wlclient.api.WLFailResponse;
import com.worklight.wlclient.api.WLResponse;
import com.worklight.wlclient.api.WLResponseListener;

/**
 * Created by manishp on 10/11/15.
 */
public class ResponseHandler implements WLResponseListener {

    private final String TAG = ResponseHandler.class.getSimpleName();
    private final ServerUtil.OnResponseListener mOnResponseListener;

    public ResponseHandler(ServerUtil.OnResponseListener onResponseListener) {
        this.mOnResponseListener = onResponseListener;
    }

    @Override
    public void onSuccess(WLResponse wlResponse) {
        Log.e(TAG, "RESPONSE : " + wlResponse.getResponseJSON().toString());
        mOnResponseListener.onSuccess(wlResponse.getResponseJSON().toString());
    }

    @Override
    public void onFailure(WLFailResponse wlFailResponse) {
        Log.e(TAG, "ERROR RESPONSE : " + wlFailResponse.getErrorMsg() + " - " + wlFailResponse.getErrorCode().name());
        mOnResponseListener.onFailure(wlFailResponse.getErrorMsg());
    }

}
