package com.ideacellular.ideamoney.utils;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.ideacellular.ideamoney.IdeaMoneyApplication;

/**
 * Created by avanin on 21/4/16.
 */
public class PrefsManager {
    private static SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(IdeaMoneyApplication.getInstance());
    }

    public static void setPref(String key, String value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString(key, value);
        editor.apply();

    }

    public static String getStringPref(String key) {
        return getSharedPreferences().getString(key, null);
    }
}
